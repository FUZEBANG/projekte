package controller;
import model.MySQLDatabase;
import model.Rechteck;
import view.RechteckGUI;

import java.util.LinkedList;

public class BunteRechteckeController {

    private LinkedList<Rechteck> rechtecke;
    private MySQLDatabase database;

    public BunteRechteckeController() {
        rechtecke = new LinkedList<>();
        this.database = new MySQLDatabase();
        rechtecke = this.database.getAlleRechtecke();
    }

    public void add(Rechteck rechteck) {
       rechtecke.add(rechteck);
       this.database.rechteckEintragen(rechteck);
    }

    public void generiereZufallsRechteck(int anzahl) {
        reset();
        for (int i = 0; i < anzahl; i++) {
            rechtecke.add(Rechteck.generiereZufallsRechteck());
        }
    }

    public void reset() {
        rechtecke = new LinkedList<>();
    }

    public static void main(String[] args) {

    }

    @Override
    public String toString() {
        return "BunteRechteckeController[" +
                "rechtecke=" + rechtecke +
                ']';
    }

    public LinkedList<Rechteck> getRechtecke() {
        return rechtecke;
    }

    public void rechteckHinzufuegen() {
        new RechteckGUI(this);
    }
}
