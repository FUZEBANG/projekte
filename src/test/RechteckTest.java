package test;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.util.Arrays;

public class RechteckTest {

    public static void Setter() {
        int[][] Array = {
                { 10, 10, 30, 40},
                { 25, 25, 100, 20},
                { 260, 10, 200, 100},
                { 5, 500, 300, 25},
                { 100, 100, 100, 100}
        };

        for (final int[] zahlen : Array) {
            Rechteck rechteck = new Rechteck();
            rechteck.setX(zahlen[0]);
            rechteck.setY(zahlen[1]);
            rechteck.setBreite(zahlen[2]);
            rechteck.setHoehe(zahlen[3]);

            equalizer(zahlen, rechteck);
        }

        int[][] Array2 = {
                { 200, 200, 200, 200},
                { 800, 400, 20, 20},
                { 800, 450, 20, 20},
                { 850, 400, 20, 20},
                { 855, 455, 25, 25}
        };

        for (final int[] zahlen : Array2) {
            Rechteck rechteck = new Rechteck(zahlen[0], zahlen[1], zahlen[2], zahlen[3]);

            equalizer(zahlen, rechteck);
        }
    }

    public static Rechteck[] rechteckeTesten() {
        Rechteck[] rarray = new Rechteck[50000];
        for (int i = 0; i < rarray.length; i++) {
            rarray[i] = Rechteck.generiereZufallsRechteck();
        }
        return rarray;
    }

    private static void equalizer(int[] Zahlen, Rechteck rechteck) {
        BunteRechteckeController bunteRechteckeController = new BunteRechteckeController();
        System.out.println(rechteck.toString());
        System.out.println(rechteck.toString().equals("Rechteck[x=" + Zahlen[0] + ", y=" + Zahlen[1] + ", breite=" + Zahlen[2] + ", hoehe=" + Zahlen[3] + "]"));
        bunteRechteckeController.add(rechteck);
    }

    public static void main(String[] args) {
        // Setter();
        // System.out.println(bunteRechteckeController.toString());
        // System.out.println(bunteRechteckeController.toString().equals("BunteRechteckeController[rechtecke=[Rechteck[x=10, y=10, breite=30, hoehe=40], Rechteck[x=25, y=25, breite=100, hoehe=20], Rechteck[x=260, y=10, breite=200, hoehe=100], Rechteck[x=5, y=500, breite=300, hoehe=25], Rechteck[x=100, y=100, breite=100, hoehe=100], Rechteck[x=200, y=200, breite=200, hoehe=200], Rechteck[x=800, y=400, breite=20, hoehe=20], Rechteck[x=800, y=450, breite=20, hoehe=20], Rechteck[x=850, y=400, breite=20, hoehe=20], Rechteck[x=855, y=455, breite=25, hoehe=25]]]"));
        Rechteck rechteck = new Rechteck();
        System.out.println(Arrays.toString(rechteckeTesten()));
    }

}
