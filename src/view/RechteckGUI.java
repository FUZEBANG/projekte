package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField jtfX;
    private JTextField jtfY;
    private JTextField jtfLaenge;
    private JTextField jtfHoehe;
    private BunteRechteckeController bunteRechteckeController;

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                new RechteckGUI(new BunteRechteckeController());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public RechteckGUI(BunteRechteckeController bunteRechteckeController) {
        this.bunteRechteckeController = bunteRechteckeController;
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        JPanel pnlEingabe = new JPanel();
        contentPane.add(pnlEingabe, BorderLayout.CENTER);
        pnlEingabe.setLayout(new GridLayout(0, 1, 0, 0));

        JLabel lblX = new JLabel("X");
        pnlEingabe.add(lblX);
        jtfX = new JTextField();
        pnlEingabe.add(jtfX);
        jtfX.setColumns(25);

        JLabel lblY = new JLabel("Y");
        pnlEingabe.add(lblY);
        jtfY = new JTextField();
        pnlEingabe.add(jtfY);
        jtfY.setColumns(25);

        JLabel lblLnge = new JLabel("Laenge");
        pnlEingabe.add(lblLnge);
        jtfLaenge = new JTextField();
        pnlEingabe.add(jtfLaenge);
        jtfLaenge.setColumns(25);

        JLabel lblHoehe = new JLabel("Hoehe");
        pnlEingabe.add(lblHoehe);
        jtfHoehe = new JTextField();
        pnlEingabe.add(jtfHoehe);
        jtfHoehe.setColumns(25);

        JButton btnSpeichern = new JButton("Speichern");
        pnlEingabe.add(btnSpeichern);
        btnSpeichern.addActionListener(arg0 -> speichern());
        setVisible(true);
    }

    protected void speichern() {
        int x = Integer.parseInt(jtfX.getText());
        int y = Integer.parseInt(jtfY.getText());
        int laenge = Integer.parseInt(jtfLaenge.getText());
        int hoehe = Integer.parseInt(jtfHoehe.getText());

        Rechteck rechteck = new Rechteck(x,y, laenge, hoehe);
        bunteRechteckeController.add(rechteck);
    }
}
