package view;

import controller.BunteRechteckeController;
import model.Rechteck;

import javax.swing.*;
import java.awt.*;

public class Zeichenflaeche extends JPanel {

    private BunteRechteckeController bunteRechteckeController;

    public Zeichenflaeche(BunteRechteckeController bunteRechteckeController) {
        this.bunteRechteckeController = bunteRechteckeController;
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);

        for (Rechteck rechteck : bunteRechteckeController.getRechtecke()) {
            g.drawRect(rechteck.getX(), rechteck.getY(), rechteck.getBreite(), rechteck.getHoehe());
        }
    }
}
