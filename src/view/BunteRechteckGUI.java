package view;

import controller.BunteRechteckeController;
import model.Rechteck;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class BunteRechteckGUI extends JFrame {

    private JPanel contentPane;
    private JMenuBar menuBar;
    private JMenu menu;
    private JMenuItem menuItemNeuesRechteck;
    private BunteRechteckeController bunteRechteckeController;

    public static void main(String[] args) {
        BunteRechteckGUI frame = new BunteRechteckGUI();
        frame.run();
    }

    public void run() {
        while(true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.revalidate();
            this.repaint();
        }
    }

    public BunteRechteckGUI() {
        this.bunteRechteckeController = new BunteRechteckeController();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, 1920, 1200);
        contentPane = new Zeichenflaeche(this.bunteRechteckeController);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        this.menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        this.menu = new JMenu("Hinzufügen");
        this.menuBar.add(menu);
        this.menuItemNeuesRechteck = new JMenuItem("Rechteck hinzufügen");
        this.menu.add(this.menuItemNeuesRechteck);
        this.menuItemNeuesRechteck.addActionListener(e -> {
            menuItemNeuesRechteck_Clicked();
        });
        this.menuItemNeuesRechteck = new JMenuItem("Random");
        this.menu.add(this.menuItemNeuesRechteck);
        this.menuItemNeuesRechteck.addActionListener(e -> {
            menuItemNeuesRechteck_ClickedR();
        });
        setVisible(true);
    }

    private void menuItemNeuesRechteck_ClickedR() {
        this.bunteRechteckeController.generiereZufallsRechteck(Rechteck.randomInt(1, 1200));
    }

    private void menuItemNeuesRechteck_Clicked() {
        this.bunteRechteckeController.rechteckHinzufuegen();
    }
}
