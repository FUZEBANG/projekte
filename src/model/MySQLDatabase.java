package model;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class MySQLDatabase {

    private final String driver = "com.mysql.cj.jdbc.Driver";
    private final String url = "jdbc:mysql://localhost/rechtecke?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String user = "root";
    private final String password = "";

    public void rechteckEintragen(Rechteck rechteck) {
        try {
            // JDBC-Treiber laden
            Class.forName(driver);
            // Verbindung aufbauen
            Connection con = DriverManager.getConnection(url, user, password);
            // SQL-Anweisungen ausführen
            String sql = "INSERT INTO t_rechtecke (x, y, breite, hoehe) VALUES \n" +
                    "  (?, ?, ?, ?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, rechteck.getX());
            ps.setInt(2, rechteck.getY());
            ps.setInt(3, rechteck.getBreite());
            ps.setInt(4, rechteck.getHoehe());
            ps.executeUpdate();
            // Verbindung schließen
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LinkedList<Rechteck> getAlleRechtecke() {
        LinkedList rechtecke = new LinkedList<Rechteck>();
        try {
            // JDBC-Treiber laden
            Class.forName(driver);
            // Verbindung aufbauen
            Connection con = DriverManager.getConnection(url, user, password);
            //Statement
            String sql = "SELECT * FROM T_Rechtecke;";
            ResultSet resultSet = con.createStatement().executeQuery(sql);
            //Ergebnis abfangen
            while (resultSet.next()) {
                int x = resultSet.getInt("X");
                int y = resultSet.getInt("Y");
                int breite = resultSet.getInt("Breite");
                int hoehe = resultSet.getInt("Hoehe");
                rechtecke.add(new Rechteck(x, y, breite, hoehe));
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rechtecke;
    }
}
