package model;

import java.util.Random;

public class Rechteck {

    private Punkt punkt;
    private int breite;
    private int hoehe;

    public Rechteck() {
        this.punkt = new Punkt();
        this.breite = 0;
        this.hoehe = 0;
    }

    public Rechteck(int x, int y, int breite, int hoehe) {
        this.punkt = new Punkt(x, y);
        this.breite = breite;
        this.hoehe = hoehe;
    }

    public int getX() {
        return punkt.getX();
    }

    public void setX(int x) {
        this.punkt.setX(x);
    }

    public int getY() {
        return this.punkt.getY();
    }

    public void setY(int y) {
        this.punkt.setY(y);
    }

    public int getBreite() {
        return breite;
    }

    public void setBreite(int breite) {
        this.breite = Math.abs(breite);
    }

    public int getHoehe() {
        return hoehe;
    }

    public void setHoehe(int hoehe) {
        this.hoehe = Math.abs(hoehe);
    }

    @Override
    public String toString() {
        return "Rechteck[" +
                "x=" + punkt.getX() +
                ", y=" + punkt.getY() +
                ", breite=" + breite +
                ", hoehe=" + hoehe +
                ']';
    }

    public static int randomInt(int min, int max) {
        Random random = new Random();
        int randomnumber = random.nextInt(((max - min) + 1) + min);
        return randomnumber;
    }

    public boolean enthaelt(int x, int y) {
        return x > this.punkt.getX() && x < this.punkt.getX() + breite && y > this.punkt.getY() && y < this.punkt.getY() + hoehe;
    }

    public boolean enthaelt(Punkt punkt) {
        return enthaelt(punkt.getX(), punkt.getY());
    }

    public boolean enthaelt(Rechteck rechteck) {
        return enthaelt(rechteck.punkt.getX(), rechteck.punkt.getY()) && enthaelt(rechteck.punkt.getX() + rechteck.breite, rechteck.getY() + rechteck.hoehe);
    }

    public static Rechteck generiereZufallsRechteck() {
        while (true) {
            Rechteck rechteckBound = new Rechteck(0, 0, 1200, 1000);
            Rechteck rechteck = new Rechteck();
            rechteck.setX(randomInt(0, 1200));
            rechteck.setY(randomInt(0, 1000));
            rechteck.setBreite(randomInt(rechteck.getX(), 1200));
            rechteck.setHoehe(randomInt(rechteck.getY(), 1000));
            if (rechteckBound.enthaelt(rechteck)) {
                return rechteck;
            }
        }
    }
}
